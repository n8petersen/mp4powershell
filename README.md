

## Instructions of use:
### Introduction
I created this script with the goal of saving space in my google photos. It might fit your use case or it may not. Make sure to read through the script and understand what it is doing before running.

The script makes use of [ffmpeg](https://ffmpeg.org) tools (ffmpeg and ffprobe).

The script primarily does the following:
1. Resize the video to have width of 1920, or 1080 (depending on orientation)
2. Convert the video to use mp4, with h.265 encoding (libx265 encoder)
3. Retains global metadata attributes, including location, creationdate, and others.
   - Grabs it from the input file and applies it to the output file
4. Repeats for all files in the 'input' folder

### Folder Structure:
When the repo is cloned, you should create a folder called "input" and a folder called "output", so that the folder has the following items in it:  
![Folder Structure](instructions/FolderStructure.png)  
The other files and folders are not needed for operation, but will be present when you clone the repository.

Put any video files in the 'input' folder that you would like converted.

### Operation
1. Place files to convert in the 'input' folder
2. Run the script
   - You may need to allow PowerShell scripts to run first.
   - Run `Get-ExecutionPolicy` to see the current setting, and take note of it
   - Run `Set-ExecutionPolicy Bypass` in an elevated PowerShell session
3. Verify converted files in the 'output' folder
   - (After operating the script, set the policy pack to its original state using `Set-ExecutionPolicy <old state>` in an elevated session)
