$currFolder = Get-Location
$inFolder = $currFolder.Path + "\input"
$outFolder = $currFolder.Path + "\output"

Get-ChildItem $inFolder | ForEach-Object {
    $fileInName = $_.PSChildName
    $fileIn = $inFolder + "\" + $fileInName
    # Rename file to have mp4 extension
    $fileNameNoExt = $fileInName.Substring(0, $fileInName.IndexOf("."))
    $fileExt = $fileInName.Substring($fileInName.IndexOf(".") + 1)
    $fileOutName = $fileNameNoExt + ".mp4"
    $fileOut = $outFolder + "\" + $fileOutName

    # Append Number to end of needed
    $i = 1
    while (Test-Path $fileOut) {
        $fileOutName = $fileNameNoExt + "_" + $i + ".mp4"
        $fileOut = $outFolder + "\" + $fileOutName
        $i += 1
    }

    # remove any existing metadata file
    if (Test-Path ./metadata.txt) {
        Remove-Item ./metadata.txt
    }

    try {
        
        # get metadata
        $creation_time = ffprobe -v quiet -select_streams v:0 -show_entries format_tags=creation_time -of default=noprint_wrappers=1:nokey=1 $fileIn
        if ($fileExt -eq "AVI") {
            # creation_time example: 2020-11-26T06:28:23.000000Z
            # $creation_time = $creation_time.Substring(0, $creation_time.IndexOf("/")) + "T12:00:00.000000Z"

            $creation_year = $creation_time.Substring(0, 4)
            $creation_month = $creation_time.Substring(5, 2)
            $creation_day = [int]$creation_time.Substring(8, 2)
            $creation_hour = [int]$creation_time.Substring($creation_time.IndexOf("/") + 1, 3)
            $creation_min = $creation_time.Substring($creation_time.IndexOf("/") + 5, 2)

            $creation_hour += -7

            if ($creation_hour -lt 0) {
                $creation_hour = 24 + $creation_hour
                $creation_day += -1
            }

            if ($creation_hour -lt 10) {
                $creation_hour = "0${creation_hour}"
            }

            if ($creation_day -lt 10) {
                $creation_day = "0${creation_day}"
            }

            $creation_time = "${creation_year}-${creation_month}-${creation_day}-T${creation_hour}:${creation_min}:00.000000Z"
        }
        else {
            ffmpeg -i $fileIn -c copy -map_metadata 0 -map_metadata:s:v 0:s:v -map_metadata:s:a 0:s:a -f ffmetadata metadata.txt
        }
        
        # set width of output video
        $inWidth = ffprobe -v error -select_streams v:0 -show_entries stream=width -of default=nw=1:nk=1 $fileIn
        $inHeight = ffprobe -v error -select_streams v:0 -show_entries stream=height -of default=nw=1:nk=1 $fileIn
        $inWidth = [int]$inWidth
        $inHeight = [int]$inHeight


        if ($inWidth -gt 1920) {
            if ($inWidth -gt $inHeight) {
                $width = "1920"
            }
            else {
                $width = "1080"
            }
        }
        else {
            $width = [string]$inWidth
        }

        # convert the video
        if (Test-Path ./metadata.txt) {
            ffmpeg -i $fileIn -f ffmetadata -i ./metadata.txt -c copy -map_metadata 1 -vf "scale=${width}:-2" -c:v libx265 -metadata creation_time=$creation_time $fileOut
        }
        elseif ($creation_time) {
            ffmpeg -i $fileIn -vf "scale=${width}:-2" -c:v libx265 -metadata creation_time=$creation_time $fileOut
        } else {
            ffmpeg -i $fileIn -vf "scale=${width}:-2" -c:v libx265 $fileOut
        }
        

        # print the output file name
        Write-Host "Wrote new file to:  $fileOut"

       
    }
    catch {
        write-host "oopsie!"
    }
    finally {
        # cleanup metadata file
        if (Test-Path ./metadata.txt) {
            Remove-Item ./metadata.txt
        }
    }
}